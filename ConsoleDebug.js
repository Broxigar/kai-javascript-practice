//log with Computed Property Names
const foo = {name:'tom',age:'18',height:'180'};
const bar = {name:'dick',age:'19',height:'178'};
const baz = {name:'tag',age:'20',height:'167'};

console.log({foo, bar,baz})


//custom CSS styling log
console.log('%c Hello','color:blue; font-weight:bold')

//log table
console.table([foo,bar,baz])


//check performance using console.time
console.time('performance')
let i =0;
while ( i < 1000000 ) { i++ }
console.timeEnd('performance')


//stack trace logs
const deleteMe = () => console.trace('trace multiple logs');
deleteMe()
deleteMe()


