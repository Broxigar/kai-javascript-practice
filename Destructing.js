// ${name}${string}
const turtle ={
    name:'Bob',
    age: '2',
    legs:'4',
    shell:true,
    meal:10,
}

 feed = (animal) => {
    const {name,age,meal}=animal;
    return `Feed ${name} ${age} Kilos of ${meal}`
}

console.log(feed(turtle))

// [...object,string,string,string] compose a new object from left to right
const pikachu = { name: 'mouse'};
const status = { age: '1',height:'20'};

const lv10 ={...pikachu,...status};
const lv11={...pikachu, age:'2'};
//push Array
let pikachu = [ 'Pikachu'];
let pokemon = ['Richu','Geda'];
pokemon = [...pokemon, 'Richu','Geda'];
pikachu = ['Pikachu',...pokemon];

//Loops
const numbers =[600,200,13,2,44,4];
//Reduce:
const total = numbers.reduce((acc, cur) => acc + cur);
//Map
const withTimes = numbers.map(v=> v*1.2);
//Filter
const filter = numbers.filter(value => value > 100);



