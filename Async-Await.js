
const random = ()=>{
    return Promise.resolve( Math.random())
}

//Promise async-await
const AddUpRandomNumbers = async () =>{
    const first = await random();
    const second = await random();
    const third = await random();

    console.log( `Total is ${first + second + third}`);
}
